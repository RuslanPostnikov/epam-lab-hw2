const {Router} = require('express');
const Note = require('../../models/note.model');
const logger = require('../../logger');
const router = Router();
const authJwt = require('../middleware/authJwt');

router.delete('/api/notes/:id', authJwt, async (req, res, next) => {
  try {
    const id = req.params.id;
    const userId = req.userId;
    if ( id && userId) {
      const note = await Note.findOneAndDelete({_id: id, userId});
      logger.info(note);

      if (!note) {
        return res.status(400).json({message: `No notes with requested id ${req.params.id}`});
      }

      res.type('application/json').json({'message': 'Success'});
    } else {
      res.status(404).json({message: 'You need to specify note'});
    }
  } catch (e) {
    next(e);
  }
});

module.exports = router;
