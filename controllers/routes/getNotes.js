const {Router} = require('express');
const router = Router();
const Note = require('../../models/note.model');
const logger = require('../../logger');
const authJwt = require('../middleware/authJwt');

router.get('/api/notes', authJwt, async (req, res, next) => {
  try {
    const offset = +req.query.offset;
    const limit = +req.query.limit;

    const notes = await Note
        .find({userId: req.userId})
        .skip(offset)
        .limit(limit)
        .select('-__v');

    if (!notes) return res.status(400).json({message: `No notes in database`});

    const count = await Note.countDocuments({userId: req.userId});

    logger.info(notes);

    res.type('application/json').json({
      offset,
      limit,
      count,
      notes,
    });
  } catch (e) {
    next(e);
  }
});

module.exports = router;
