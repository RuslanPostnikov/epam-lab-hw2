import axios from '../shared/axios';
import {useEffect, useState} from 'react';
import Loader from './UI/Loader';
import {useAuth} from '../shared/AuthContext';

const Profile = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [user, setUser] = useState('');
  const {setAuth} = useAuth();

  const getUser = async () => {
    try {
      const {data} = await axios.get('users/me');
      setUser(data.user);
      setIsLoading(false);
    } catch (e) {
      console.log(e);
    }
  };

  const deleteUser = async () => {
    try {
      localStorage.removeItem('jwt_token');
      localStorage.removeItem('auth');
      setAuth(false);

      await axios.delete('users/me');
      alert('Profile was deleted');
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getUser();
  }, []);

  const inputStyle = {
    color: 'yellow',
    fontSize: 20,
    background: 'transparent',
    textAlign: 'center',
    border: 0,
  };

  const renderUser = () => {
    if (isLoading) {
      return <Loader />;
    } else {
      return (
        <form className={'d-flex flex-column align-items-start gap-2'}>
          <label htmlFor="username">
            Username
            <input type="text" name={'username'} id={'username'} value={user.username} readOnly style={inputStyle} />
          </label>
          <label htmlFor="date">
            Created date
            <input type="text" name={'date'} id={'date'} value={user.createdDate} readOnly style={inputStyle} />
          </label>
          <button
            type={'submit'}
            className={'btn btn-danger align-self-center'}
            onClick={deleteUser}
          >Delete profile</button>
        </form>
      );
    }
  };

  return (
    <div className={'d-flex justify-content-center align-items-center flex-column'}>
      {renderUser()}
    </div>
  );
};

export default Profile;
